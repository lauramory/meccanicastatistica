import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

# %matplotlib inline

def get_traj(length=100, dim=2):
    """Generate a RW in d dimensions

    Parameters
    ----------
    length: int
        Length of the RW.
    dim: int
        Dimension of the RW


    Returns
    -------
    traj : np.ndarray, (length, dim)
        The positions of the RW.

    Notes
    -----
    At each time-step, the walker moves in only one direction.
    At each time-step, the walker moves by -1 or +1
    """
    possible_value = [-1, 1]
    traj = np.zeros((1, dim), dtype='int')
    new_row = np.zeros((1, dim), dtype='int')

    for step in range(1, length):
        column = np.random.randint(0, dim)
        new_row[0, column] += np.random.choice(possible_value)
        traj = np.concatenate([traj, new_row])

    return traj

mpl.rcParams['axes.titlesize'] = 16
mpl.rcParams['axes.labelsize'] = 14
mpl.rcParams['legend.fontsize'] = 12
mpl.rcParams['xtick.labelsize'] = 12
mpl.rcParams['ytick.labelsize'] = 12
mpl.rcParams['axes.spines.right'] = False
mpl.rcParams['axes.spines.top'] = False

get_traj(length=500, dim=5)

# basic checks for your RW generator
for dim in range(1, 5):
    for length in [10, 100, 200, 500]:
        traj = get_traj(length=length, dim=dim)
        # make sure traj has the right shape
        assert traj.shape == (length, dim)
        # make sure all steps are -1 or 1 in only one direction
        assert np.all(np.sum(np.diff(traj, axis=0) != 0, axis=1) == np.ones(length - 1))