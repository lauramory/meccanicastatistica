import math
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.stats import poisson

def get_waiting_times(size, lam):
    w_times = np.random.exponential(lam, size)
    return w_times

def get_event_times(size, lam):
    waiting_times = get_waiting_times(size, lam)
    # we simply do a cumulative sum of the waiting times
    # find a numpy function that does cumulatives sums
    event_times = np.cumsum(waiting_times)
    return event_times

def count_events(time_period, lam):
    # make sure everything is fine
    assert time_period > 0
    assert lam > 0
    # estimate number of events necessary
    num_events = int(poisson(mu=time_period * lam).isf(1e-6))
    # uses the inverse survival function isf...
    # generate event times
    event_times = get_event_times(num_events, lam)
    # make sure that the last event is outside the time period
    # you can access the last element of an array 'x' with 'x[-1]'
    assert event_times[-1] > time_period
    # see which events are inside the time period
    event_inside = event_times < time_period
    # count how many are inside
    num_inside = len(event_times)
    return num_inside

########################## MAIN ##########################
mpl.rcParams['axes.titlesize'] = 16
mpl.rcParams['axes.labelsize'] = 14
mpl.rcParams['legend.fontsize'] = 12
mpl.rcParams['xtick.labelsize'] = 12
mpl.rcParams['ytick.labelsize'] = 12
mpl.rcParams['axes.spines.right'] = False
mpl.rcParams['axes.spines.top'] = False

get_event_times(10, 5)
########################## MAIN ##########################