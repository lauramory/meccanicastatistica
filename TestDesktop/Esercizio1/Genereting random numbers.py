import math
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

# Write a function that gives the density of a uniform random
# variable between 𝑎 and 𝑏 at an arbitrary point 𝑥.
def uniform_pdf(x, a=0, b=1):
    if x < a:
        return 0
    elif x > b:
        return 0
    else:
        return 1 / (b - a)

# The function will have 3 arguments: the sample size N
# and the bounds a,b of the uniform random variable.

# N = numero di campioni
# a = inizio intervallo
# b = fine intervallo
def plot_comparison(N, a, b):
    # generate the random data
    data = np.random.uniform(a, b, N)

    # compute the theoretical distribution
    dx = (b - a) / 10
    x = np.linspace(a - dx, b + dx, num=N)

    pdf_x = np.array([
        uniform_pdf(t, a=a, b=b)
        for t in x
    ])

    # generate a figure with a single subplot, of some size
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))

    # plot empirical estimation
    ax.hist(
        data,
        bins="auto",
        density=True
    )

    # plot theoretical prediction
    ax.plot(
        x, pdf_x,
        color="red",
        lw=2
    )

    ax.set_xlabel(r"$x$")
    ax.set_ylabel(r"Density, $f(x)$")

    # add legend
    ax.legend(["x", "Density"])

    # add title
    ax.set_title(f"The uniform distribution U(a, b): $a$={a}, $b$={b}, $N$={N}")

    plt.show()


########################## MAIN ##########################
mpl.rcParams['axes.titlesize'] = 16
mpl.rcParams['axes.labelsize'] = 14
mpl.rcParams['legend.fontsize'] = 12
mpl.rcParams['xtick.labelsize'] = 12
mpl.rcParams['ytick.labelsize'] = 12
mpl.rcParams['axes.spines.right'] = False
mpl.rcParams['axes.spines.top'] = False

# Verify that, if the sample size increase, our empirical
# estimation gets closer to the theoretical prediction.

# Do so for the following cases:
ProfCases = [(0, 1), (-3, -1.2), (1800, 1990)]
Samples = [100, 300, 1000, 3000, 10000]

for (a, b) in ProfCases:
    for N in Samples:
        plot_comparison(N, a, b) # verra chiamata 15 volte
########################## MAIN ##########################