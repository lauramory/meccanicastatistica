import math
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

def inv_cdf_expo(y):
    # The inverse of y = 1 - exp(-x)
    x = -math.log(1-y)
    return x

def inverse_sampling(size, inv_cdf):
    # generate uniform data of given_size
    # u is a vector
    u = np.random.uniform(size=size)

    # inverse transform it
    x = np.array([
        inv_cdf(t)
        for t in u
    ])
    return x

x = np.linspace(0, 8, num=200)
pdf_x = np.array([
    math.exp(-t)
    for t in x
])

data = inverse_sampling(1000, inv_cdf_expo)

fig, ax = plt.subplots(1, 1, figsize=(10, 8))

ax.hist(
    data,
    bins= "auto",
    density=True
)

ax.plot(
    x, pdf_x,
    color="red",
    lw=2
)
ax.set_yscale("log")

plt.show()