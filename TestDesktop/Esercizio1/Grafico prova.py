import matplotlib.pyplot as plt
import numpy as np

x = np.random.normal(18, 34, 18)

plt.hist(x)  # `density=False` would make counts
plt.show()

