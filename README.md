# Esercizi computazionali di "Introduzione alla Fisica Statistica"/"Meccanica Statistica 1".
Gli esercizi in questa repository sono parte integrante dei suddetti corsi al Dipartimento di Fisica dell'Univestità degli Studi di Milano.
Sono stati preparati in origine dal Dott Francesc Font-Clos e aggiornati da C. Barbieri per il **secondo semestre, a.a. 2019/20**.

La componente computazionale del corso usa *python 3.x* e fogli note (notebooks) di *jupyter*. Ogni notebook approfondisce un particolare argomento trattato a lezione e propone degli esercizi **da completare autonomamente**.
Siccome non sarà possibile accedere fisicamente al laboratorio di calcolo per le lezioni, i notebook verrnno descritti e spiegati durante le normali lezioni (telematiche) di esercitazione e verranno lasciate alcune settimane di tempo per risolverli. 

I probemi risolti (due notebook per ogni sessione) vanno spediti **entro la data indicata qui sotto** a `carlo.barbieri@unimi.it`. I nomi dei file `.ipynb` vanno cambiati aggiungendo il proprio nome, per esempio: *1.1-Generating-Random-Numbers-**Mario-Rossi**.ipynb*. 

|            Sessione                   |   Consegna entro   |
|---------------------------------------|--------------------|
| sessione-1-introduzione-e-probabilita | 2020-05-11, 12:00h |
| sessione-2-cammini-aleatori           | 2020-06-03, 12:00h |
| sessione-3-Modello-di-Ising           | (da comunicare)    |

La risoluzione degli esercizi computazionali è *facoltativa*. Tuttavia ogni sessione completata e consegnata entro la scadenza vale un punto all'esame (massimo tre voti).

Per accedere alla repository: [https://gitlab.com/craolus/MeccStat-sessioni-python.git](https://gitlab.com/craolus/MeccStat-sessioni-python.git).


## Come aggiornare la copia locale della repository
Man mano che il nuovo materiale viene aggiunto durante il semestre, si può aggiornare la copia locale della repository come segue (se già clonata come descritto più avanti):

```bash
$ cd stat-mech-python-course
$ git pull
```
È bene ricordarsi di **cambiare il nome dei file di notebook** su sui si sta lavorando, per non sovrascriverne il contenuto.


## Primi Passi
Aprire un terminale e muoversi nella cartella in cui si intende lavorare (`cd` in unix/linux):
```bash
$ cd My_folder
```
Dai computer del *Laboratorio di Calcolo* in dipartimento, è necessario attivare l'istallazione di anaconda 
```bash
$ module load python3/anaconda
```
Sul proprio computer, conviene controllare che Anaconda e python siano presenti 
```bash
$ which python
/home/username/anaconda3/bin/python
```
Per clonare la repository
```bash
$ git clone git@gitlab.com:craolus/MeccStat-sessioni-python.git
```
Il comando sopra crea una cartella `MeccStat-sessioni-python`. Una volta entrati nella caterlla, jupiter viene invocato digitando `jupyter lab` da terminale:
```bash
$ cd MeccStat-sessioni-python
$ jupyter lab
```
Questo apre una nuova tavola nel browser all'indirizzo `localhost:8888`. Cliccando sulla cartella `sessione-1-introzione-alle-probabilita` e poi su `1.1-Generating-Random-Numbers.ipynb`, si è pronti ad iniziare!


## Istallare jupyter sul proprio computer
Risovere gli esercizi richiede una versione recente di `python`, che includa `jupyter`, `numpy`, `matplotlib` e alcune altre librerie standard in python. Se queste non sono presenti, la soluzione migliore per evitare di interferire con eventuali python già presenti nel sistema operativo è quella di istallare [Anaconda](https://www.anaconda.com/download/), la quale mantiene una distribuszione completa e aggiornata di python e relativi strumenti per vari sistemi operativi.  Dal sito qui sopra scegliere **python 3.x** ed il proprio OS per scaricare ed istallare il tutto. 


## Ricerca di esempi in rete
In certe situazioni può diventare utile ed efficiente **riadattare codici altrui** o prenderne esempio (purché la paternità del codice venga propriamente riconosciuta!). Nel risolvere questi esercizi, *non* è richiesto rifare tutto da capo, cercare informazioni in reter su *how to do X in python* è ammesso. Alcuni siti che riportano informazioni utili sono:

+ Un buon forum dove cercare informazioni di natura computazionale:
  + [Stackoverflow](https://stackoverflow.com/)

+ Documentazioni ufficiali:
  + [Numpy documentation](https://docs.scipy.org/doc/numpy/reference/routines.html)
  + [Scipy documentation](https://docs.scipy.org/doc/scipy/reference/)
  + [Jupyter Lab documentation](https://jupyterlab.readthedocs.io/en/stable/)

+ Tutorial per Pyhon:
  + [Data Flair Python Tutorials](https://data-flair.training/blogs/python-tutorials-home/)
  
## License
Copyright 2020 (c) Laura Antonella Mory. All rights reserved.

Licensed under the [MIT](LICENSE) License.